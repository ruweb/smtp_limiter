#!/bin/sh
exec 2>&1
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
echo "<PRE>"
set -e

set -x

echo "action=update&value=program" >> /usr/local/directadmin/data/task.queue

set +ex
echo "<b>Plugin has been updated!</b>"; #NOT! :)

cd $DOCUMENT_ROOT/.. && chown -hR diradmin:diradmin *

exit 0;
