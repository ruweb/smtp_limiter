#!/bin/sh
exec 2>&1
export PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
echo "<PRE>"
set -xe

perl -ni -e 'unless (m/#Added by SMTP Limiter Plugin$/) { print }' /etc/crontab 2>&1

set +e
#rm /etc/virtual/.smtp_deny/* 2>/dev/null
#rm /etc/virtual/.smtp_usage/* 2>/dev/null
set +x
date >> /root/smtp_limiter_uninstall.log
if [ "`hostname|grep '.deserv.net$'`" = "`hostname`" ]; then
    EXIMV=`exim -bV | grep '^Exim version' | tr _ ' ' | awk '{print $3}'`
    if [ 0$(echo "0$EXIMV < 4.82"|bc) -eq 1 ]; then
	echo EXIM VERSION: $EXIMV LOWER THAN 4.82!
	echo PLEASE UPGRADE AND RUN UNINSTALL AGAIN
	exit 1;
    fi
    sh $DOCUMENT_ROOT/update-skins.sh 2>&1 | tee -a /root/smtp_limiter_uninstall.log
    sh $DOCUMENT_ROOT/new_conf.sh 2>&1 | tee -a /root/smtp_limiter_uninstall.log || (echo "Failed to auto-update exim configuration. PLEASE UPDATE MANUALLY AND RUN UNINSTALL AGAIN"; exit 1) || exit 1
fi
sh $DOCUMENT_ROOT/convert_limits.sh 2>&1 | tee -a /root/smtp_limiter_uninstall.log
echo "Plugin Un-Installed!"; #NOT! :)

exit 0;
