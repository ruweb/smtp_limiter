#!/bin/sh
exec 2>&1
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
echo "<PRE>"
set -xe

set +xe
echo "<b>Plugin Installed!<b>"; #NOT! :)

cd $DOCUMENT_ROOT/.. && chown -hR diradmin:diradmin *

exit 0;
