#!/bin/sh
su -m mail -c "mkdir -p -m750 /var/spool/exim/db; /usr/local/bin/sqlite3 /var/spool/exim/db/greylist.db" <<EOF
.timeout 5000
CREATE TABLE resenders (
        host            TEXT,
        helo            TEXT,
        time            INTEGER,
    PRIMARY KEY (host, helo) );
CREATE TABLE greylist (
        id              TEXT,
        expire          INTEGER,
        host            TEXT,
        helo            TEXT);
CREATE INDEX id on greylist (id);
EOF
#chown mail:mail /var/spool/exim/db/greylist.db