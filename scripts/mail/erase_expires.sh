#!/bin/sh
if [ -r /var/spool/exim/db/greylist.db ]; then
GREYEXPIRE=`grep "^GREYEXPIRE=[0-9]" /etc/exim-greylist.conf.inc`
let "$GREYEXPIRE" >/dev/null
/usr/local/bin/sqlite3 /var/spool/exim/db/greylist.db <<EOF
.timeout 5000
DELETE FROM greylist WHERE expire < $((`date +%s` - $GREYEXPIRE));
DELETE FROM resenders WHERE time < $((`date +%s` - 604800));
vacuum;
EOF
ERR=$?
else
ERR=2
fi

if [ $ERR -gt 0 ]; then
    . /etc/mail/greylist_create_db.sh
fi
