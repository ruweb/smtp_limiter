#!/usr/bin/perl 

#smtpauth
#called by exim to verify if an smtp user is allowed to
#send email through the server
#possible success:
# user is in /etc/virtual/domain.com/passwd and password matches
# user is in /etc/passwd and password matches in /etc/shadow

my $version=0.95;

our $disable_smtp_after_limit=1;
our $block_smtp_after_2day=1;
our $notify_admin='support@ruweb.net';
our $copy_admin='support@ruweb.net';
our $apacheuser='(apache|www)';

sub smtpauth
{
	my $username		= Exim::expand_string('$1');
	my $password		= Exim::expand_string('$2');
	my $extra		= Exim::expand_string('$3');
	my $domain		= "";
	my $unixuser	= 1;

	#check for netscape that offsets the login/pass by one
	if (length($extra) > 0 )
	{
		if ($username eq "" || $username eq $password)
		{
			$username = $password;
			$password = $extra;
		}
	}

	if ($username =~ /\@/)
	{
		$unixuser = 0;
		($username,$domain) = split(/\@/, $username);
		if ($domain eq "") { return "no"; }
	}

	if ($unixuser == 1)
	{
		#the username passed doesn't have a domain, so its a system account
		my $homepath = (getpwnam($username))[7];
		if ($homepath eq "") { return 0; }
		open(PASSFILE, "< $homepath/.shadow") || return "no";
		my $crypted_pass = <PASSFILE>;
		close PASSFILE;

		if ($crypted_pass eq crypt($password, $crypted_pass)) { return "yes"; }
		else { return "no"; }
	}
	else
	{
		#the username contain a domain, which is now in $domain.
		#this is a pure virtual pop account.

		open(PASSFILE, "< /etc/virtual/$domain/passwd") || return "no";
		my $test_user;
		my $test_pass;
		while (<PASSFILE>)
		{
			($test_user,$test_pass) = split(/:/,$_);
			$test_pass =~ s/\n//g; #snip out the newline at the end
			if ($test_user eq $username)
			{
				if ($test_pass eq crypt($password, $test_pass))
				{
					close PASSFILE;
					return "yes";
				}
			}
		}
		close PASSFILE;
		return "no";
	}

	return "no";
}

sub get_domain_owner
{
	my ($domain) = @_;
	my $username="";
	open(DOMAINOWNERS,"/etc/virtual/domainowners");
	while (<DOMAINOWNERS>)
	{
		$_ =~ s/\n//;
		my ($dmn,$usr) = split(/: /, $_);
		if ($dmn eq $domain)
		{
			return $usr;
		}
	}
	close(DOMAINOWNERS);

	return '';
}

sub find_user_auth_id
{
	# this will be passwed either
	# 'username' or 'user@domain.com'

	my ($auth_id) = @_;
	my $domain = "";
	my $user = "";
	my $username = $auth_id;
	my @pw;

	if ($auth_id =~ /\@/)
	{
		($user,$domain) = split(/\@/, $auth_id);
		if ($domain eq "") { return ''; }

		# we need to take $domain and get the user from /etc/virtual/domainowners
		# once we find it, set $username
		my $u = get_domain_owner($domain);
		#log_str("get_domain_owner returns '$u'\n");
		if ($u)
		{
			$username = $u;
		}
	}

	#log_str("username found from $auth_id: $username:\n");

	return $username;
}

sub find_user_sender
{
	my $sender_address = Exim::expand_string('$sender_address');
	my $primary_hostname = Exim::expand_string('$primary_hostname');
	
	#log_str("sender_address: $sender_address\n");

	my ($user,$domain) = split(/\@/, $sender_address);
	
	my $username='';

	if ($domain eq $primary_hostname) {
	    $username = $user;
	} else {
	    $username = get_domain_owner($domain);
	}

	return $username;
}

sub find_user
{
	my $auth_id;
	my $user;
	unless (($auth_id)=@_) { $auth_id = Exim::expand_string('$authenticated_id'); }
	
	if ($auth_id) {
	    $user = find_user_auth_id($auth_id);
#	    log_str("find_user_auth_id($auth_id) returns '$user' \n");
	} elsif (Exim::expand_string('$sender_ident')  =~ /^$apacheuser$/) {
	    # this shouldn't be used if safe_mode is disabled in mod_php
	    $user = find_user_sender;
	}
#	log_str("Found user: '$user'\n");
	return $user;
}

sub user_exempt
{
        my ($name) = @_;
        if ($name eq "root") { return 1; }
        if ($name eq "diradmin") { return 1; }
        return 0;
}


#check_limits
#used to enforce limits for the number of emails sent
#by a user.  It also logs the bandwidth of the data
#for received mail

sub check_limits
{
	my $count = 0;
	my $email = '';

	#find the curent user
#	my $user = find_user();
	my ($user,$mailbox) = @_;
	
	#log_str("Checking user: '$user'\n");
	#log_str("Checking mailbox: '$mailbox'\n");
	
	if (!$user) { return 1; }

	if (user_exempt($user)) { return 0; }

	my $sender_address 	= Exim::expand_string('$sender_address');
	my $authenticated_id	= Exim::expand_string('$authenticated_id');
	my $sender_host_address	= Exim::expand_string('$sender_host_address');
	my $mid 		= Exim::expand_string('$message_id');
	my $message_size	= Exim::expand_string('$message_size');
	my $local_part		= Exim::expand_string('$local_part');
	my $domain		= Exim::expand_string('$domain');
	my $message_headers	= Exim::expand_string('$message_headers');
	my $timestamp		= time();
#	if ($mid eq "")	{ return "yes";	}
	umask(002);

	my $email_limit;
	if (open (LIMIT, "/etc/virtual/limit_$user")) {
		$email_limit = int(<LIMIT>);
		close(LIMIT);
	} elsif (open (LIMIT, "/etc/virtual/limit")) {
		$email_limit = int(<LIMIT>);
		close(LIMIT);	
	}

	if ($email_limit > 0) {
	    $count = (stat("/etc/virtual/usage/$user"))[7]+1;
	    if ($count > $email_limit) {
		if ($disable_smtp_after_limit && !-e "/etc/virtual/.smtp_deny/$user") {
	    	    open(DENY, ">/etc/virtual/.smtp_deny/$user");
		    close(DENY);
		}
		return 1;
#		die("You ($user) have reach your daily email limit of $email_limit emails\n");
	    } elsif ($count == $email_limit){
		#taddle on the dataskq
		#note that the sender_address here is only the person who sent the last email
		#it doesnt meant that they have sent all the spam
		#this action=limit will trigger a check on usage/user.bytes, and DA will try and figure it out.
		if (open(TQ, ">>/etc/virtual/mail_task.queue")){
			print TQ "action=limit&username=$name&count=$count&limit=$email_limit&email=$sender_address&authenticated_id=$authenticated_id&sender_host_address=$sender_host_address&log_time=$timestamp\n";
			close(TQ);
			chmod (0660, "/etc/virtual/mail_task.queue");
		}
		$email = get_user_email($user);
        	if ($notify_admin && open(MAIL,"|/usr/sbin/sendmail -t -f '<>'")){
            	    print MAIL <<END;
From: SMTP Limiter <>
To: $notify_admin
Subject: User $user have reached his daily email limit
CC: $email

User $user have reached his daily email limit ($email_limit).

END
		    if ($message_headers) {
			print MAIL "------ This is initial part of the last message, including all the headers ------\n\n";
			print MAIL $message_headers,"\n\n";
			print MAIL Exim::expand_string('$message_body'),"\n";
			close MAIL;
		    }
		}
		if ($block_smtp_after_2day && -e "/etc/virtual/.smtp_deny.prv/$user"){
	    		open(DENY, ">/etc/virtual/.smtp_deny/$user");
			close(DENY);
			$uid = getpwnam('admin');
			$gid = getgrnam('admin');
			chown $uid, $gid, "/etc/virtual/.smtp_deny/$user";
			if (open(MAIL,"|/usr/sbin/sendmail -t -f '<>'")){
				print MAIL <<END;
From: SMTP Limiter <>
To: $notify_admin
Subject: User $user have reached his daily email limit again. SMTP access is DISABLED
CC: $email
BCC: $copy_admin

User $user is blocked for repetitive excess of the limit ($email_limit).
To restore SMTP access and get additional assistance please contact technical support.

END
				close MAIL;
			}
		}
	    }
	    open(USAGE, ">>/etc/virtual/usage/$user");
	    print USAGE "1";
	    close(USAGE);
	}

	if ( ($mailbox ne "")  )
	{
		my $userbox="";
		my $domain="";
		($userbox, $domain) = (split(/@/, $mailbox));

		if ($domain ne "")
		{
			my $user_email_limit = 0;
			if (open (LIMIT, "/etc/virtual/$domain/limit/$userbox"))
			{
				$user_email_limit = int(<LIMIT>);
				close(LIMIT);
			}
			else
			{
				if (open (LIMIT, "/etc/virtual/user_limit"))
				{
					$user_email_limit = int(<LIMIT>);
					close(LIMIT);
				}
			}

			if ($user_email_limit > 0)
			{
				$count = 0;
				$count = (stat("/etc/virtual/$domain/usage/$userbox"))[7]+1;

				if ($count > $user_email_limit)
				{
					if ($disable_smtp_after_limit && !-e "/etc/virtual/.smtp_deny/$mailbox") {
	    	    				open(DENY, ">/etc/virtual/.smtp_deny/$mailbox");
		    				close(DENY);
					}
		                        return 1;
				}

				if ($count == $user_email_limit)
				{
					if (open(TQ, ">>/etc/virtual/mail_task.queue")){
						print TQ "action=userlimit&username=$user&count=$count&limit=$user_email_limit&email=$sender_address&authenticated_id=$authenticated_id&sender_host_address=$sender_host_address&log_time=$timestamp\n";
						close(TQ);
						chmod (0660, "/etc/virtual/mail_task.queue");
					}
                                        if ($notify_admin){
						if ($email eq '') { $email = get_user_email($user); }
				        	if (open(MAIL,"|/usr/sbin/sendmail -t -f '<>'")){
				            	    print MAIL <<END;
From: SMTP Limiter <>
To: $notify_admin
Subject: Mailbox $mailbox have reached his daily email limit
CC: $email

Mailbox $mailbox have reached his daily email limit ($user_email_limit).

END
						    if ($message_headers) {
							print MAIL "------ This is initial part of the last message, including all the headers ------\n\n";
							print MAIL $message_headers,"\n\n";
							print MAIL Exim::expand_string('$message_body'),"\n";
							close MAIL;
						    }
						}
					}
				}

				if (! -d "/etc/virtual/$domain/usage") { mkdir("/etc/virtual/$domain/usage", 0770); }
				if (open(USAGE, ">>/etc/virtual/$domain/usage/$userbox")){
					print USAGE "1";
					close(USAGE);
				}
			}
		}
	}

	log_bw($user,"type=email&email=$sender_address&method=outgoing&id=$mid&authenticated_id=$authenticated_id&sender_host_address=$sender_host_address&log_time=$timestamp&message_size=$message_size&local_part=$local_part&domain=$domain");

	return 0;
}

sub log_bw
{
	my ($name,$data) = @_;
	
	my $bytes		= Exim::expand_string('$message_size');
	if ($bytes == -1) { return 1; }
	my $work_path 		= $ENV{'PWD'};
	
	umask(006);
	open (BYTES, ">>/etc/virtual/usage/$name.bytes");
	print BYTES "$bytes=$data&path=$work_path\n";
	close(BYTES);
	return 1;
}


sub save_virtual_user
{
	my $dmn = Exim::expand_string('$domain');
	my $lp  = Exim::expand_string('$local_part');
	my $usr = "";
	my $pss = "";
	my $entry = "";

	open (PASSWD, "/etc/virtual/$dmn/passwd") || return 0;

	while ($entry = <PASSWD>) {
		($usr,$pss) = split(/:/,$entry);
		if ($usr eq $lp) {
			close(PASSWD);
			log_email($lp, $dmn);
			return 1;
		}
	}
	close (PASSWD);

	return 0;
}

sub log_email
{
	my($lp,$dmn) = @_;

#	log_str("logging $lp\@$dmn\n");
	my $user = get_domain_owner($dmn);
	if (!$user) { return 0; }

	my $mid = Exim::expand_string('$message_id');
	my $timestamp           = time();

	if ( (@pw = getpwnam($user))  )
	{
		log_bandwidth($pw[2],"type=email&email=$lp\@$dmn&method=incoming&log_time=$timestamp&id=$mid");
	}

	return 1;
}


sub log_bandwidth
{
	my ($uid,$data) = @_;
	my $name = getpwuid($uid);

	if ($name eq "") { return; }

	if (user_exempt($name)) { return; }

	my $bytes = Exim::expand_string('$message_size');

	if ($bytes == -1) { return; }

	my $work_path = $ENV{'PWD'};

	umask(006);
	open (BYTES, ">>/etc/virtual/usage/$name.bytes");
	print BYTES "$bytes=$data&path=$work_path\n";
	close(BYTES);
}

sub log_str
{
	my ($str) = @_;
	
	open (LOG, ">> /tmp/test.txt");

	print LOG $str;

	close(LOG);
}

sub check_maildirsize
{
    my $quota;
    my $limit;
    my ($sizefile) = @_;
    if ($sizefile) {
	open (FILE, $sizefile) || return 0;
	$_=readline (FILE);
	($limit) = (/^(\d+)S/);
	if ($limit){
	    $quota=0;
	    while (<FILE>) { ($_) = split(/\s/); $quota+=$_; }
#log_str "check_quota_virtual $sizefile: $quota,$limit\n";
	    if ($quota >= $limit) {
		$quota = (stat(FILE))[9];
		close (FILE); 
		return $quota;
	    }
	}
	close (FILE);
    }

    return 0;
}

sub get_user_email
{
	my ($user) = @_;
	if (open (CONF,"/usr/local/directadmin/data/users/$user/user.conf")) {
	    while (<CONF>) { if (~/^email=(.+)$/) {
		return $1;
	    }}
	}
	return '';
}
