#!/bin/sh

#DOCUMENT_ROOT=`pwd`

cd $DOCUMENT_ROOT

if [ ! -e /etc/exim.blockcracking ]; then
    mkdir /etc/exim.blockcracking && cp -p ./exim.blockcracking/* /etc/exim.blockcracking/
fi
cp -p ./mail/* /etc/mail/
cp -p exim-greylist.conf.inc exim.conf.update /etc/

grep '^#VERSION=' /etc/exim.pl && echo "new exim.pl found. skipping config update..." && exit

chown root:wheel exim.conf exim.pl

exim -C $DOCUMENT_ROOT/exim.conf -pv -bt andrey@ruweb.net admin || exit $?

exim -C $DOCUMENT_ROOT/exim.conf -pv -bv andrey@ruweb.net admin || exit $?

TIME=`date +%s`

mv /etc/exim.pl "/etc/exim.pl.$TIME" || exit $?
cp -p $DOCUMENT_ROOT/exim.pl /etc/exim.pl || exit $?

chflags noschg /etc/exim.conf /etc/exim.pl
mv /etc/exim.conf "/etc/exim.conf.$TIME" && cp -p $DOCUMENT_ROOT/exim.conf /etc/exim.conf
if [ $? -ne 0 ]; then
    cp -pv "/etc/exim.conf.$TIME" /etc/exim.conf
    cp -pv "/etc/exim.pl.$TIME" /etc/exim.pl
    exit 1
fi

#/usr/local/etc/rc.d/exim restart

touch /etc/virtual/blacklist_senders

killall -v -HUP -m '^exim($|4$|-4)'

chown -R root:wheel /etc/exim*
chown mail:wheel /etc/exim.key
chmod 400 /etc/exim.key
chflags schg /etc/exim.conf /etc/exim.pl

mv /usr/local/etc/dovecot.conf /usr/local/etc/dovecot.conf.$TIME \
    && cat dovecot.conf > /usr/local/etc/dovecot.conf && killall -HUP dovecot

echo -e "=== OK! New exim configuration installed ==="
echo
