#!!# This file is output from the convert4r4 script, which tries
#!!# to convert Exim 3 configurations into Exim 4 configurations.
#!!# However, it is not perfect, especially with non-simple
#!!# configurations. You must check it before running it.
exim_user=mail

#smtp_banner = $primary_hostname ESMTP $tod_full
smtp_active_hostname = ${if exists{/etc/virtual/helo_data}{${lookup{$interface_address}iplsearch{/etc/virtual/helo_data}{$value}{$primary_hostname}}}{$primary_hostname}}
smtp_banner = $smtp_active_hostname ESMTP $tod_full

accept_8bitmime = true
helo_allow_chars = _
print_topbitchars = true
daemon_smtp_ports = 25 : 587 : 465
tls_on_connect_ports = 465
#split_spool_directory = yes
#deliver_queue_load_max = 8
#queue_only_load = 5
#smtp_accept_queue = 100
#smtp_accept_reserve = 5
smtp_accept_max = 400
smtp_accept_max_per_host = 4
smtp_receive_timeout = 60s
message_body_visible = 2048
message_size_limit = 30M
bounce_return_size_limit = 10K
auto_thaw = 1d
disable_ipv6 = true

log_selector = +address_rewrite -rejected_header

#!!# These options specify the Access Control Lists (ACLs) that
#!!# are used for incoming SMTP messages - after the RCPT and DATA
#!!# commands, respectively.

acl_smtp_mail = acl_check_mail
acl_smtp_rcpt = check_recipient
#acl_smtp_data = check_message
#acl_smtp_mail = check_sender
acl_not_smtp = check_not_smtp

#!!# This setting defines a named domain list called
#!!# local_domains, created from the old options that
#!!# referred to local domains. It will be referenced
#!!# later on by the syntax "+local_domains".
#!!# Other domain and host lists may follow.

domainlist local_domains = lsearch;/etc/virtual/domains : $primary_hostname

domainlist relay_domains = lsearch;/etc/virtual/domains : \
    localhost

hostlist relay_hosts = 

hostlist auth_relay_hosts = *

addresslist blacklist_senders = lsearch;/etc/virtual/blacklist_senders
domainlist blacklist_domains = lsearch;/etc/virtual/blacklist_domains
hostlist bad_sender_hosts = net-lsearch;/etc/virtual/bad_sender_hosts

smtp_reserve_hosts = net-lsearch;/etc/virtual/reserve_hosts

# An Exim configuration for 3.33 that supports virtual email users
# with separate aliases and passwd files and mail spool locations
# for each domain.
# 21/Sep/2001 reed -- please let me know how this can be improved

######################################################################
#                  Runtime configuration file for Exim               #
######################################################################


# This is a default configuration file which will operate correctly in
# uncomplicated installations. Please see the manual for a complete list
# of all the runtime configuration options that can be included in a
# configuration file. There are many more than are mentioned here. The
# manual is in the file doc/spec.txt in the Exim distribution as a plain
# ASCII file. Other formats (PostScript, Texinfo, HTML, PDF) are available
# from the Exim ftp sites. The manual is also online via the Exim web sites.


# This file is divided into several parts, all but the last of which are
# terminated by a line containing the word "end". The parts must appear
# in the correct order, and all must be present (even if some of them are
# in fact empty). Blank lines, and lines starting with # are ignored.


############ IMPORTANT ########## IMPORTANT ########### IMPORTANT ############
#                                                                            #
# Whenever you change Exim's configuration file, you *must* remember to HUP  #
# the Exim daemon, because it will not pick up the new configuration until   #
# until you do this. It is usually a good idea to test a new configuration   #
# for syntactic correctness (e.g. using "exim -C /config/file -bV") first.   #
#                                                                            #
############ IMPORTANT ########## IMPORTANT ########### IMPORTANT ############



######################################################################
#                    MAIN CONFIGURATION SETTINGS                     #
######################################################################

# Specify your host's canonical name here. This should normally be the fully
# qualified "official" name of your host. If this option is not set, the
# uname() function is called to obtain the name.

# primary_hostname =

# Specify the domain you want to be added to all unqualified addresses
# here. An unqualified address is one that does not contain an "@" character
# followed by a domain. For example, "caesar@rome.ex" is a fully qualified
# address, but the string "caesar" (i.e. just a login name) is an unqualified
# email address. Unqualified addresses are accepted only from local callers by
# default. See the receiver_unqualified_{hosts,nets} options if you want
# to permit unqualified addresses from remote sources. If this option is
# not set, the primary_hostname value is used for qualification.

# qualify_domain =


# If you want unqualified recipient addresses to be qualified with a different
# domain to unqualified sender addresses, specify the recipient domain here.
# If this option is not set, the qualify_domain value is used.

# qualify_recipient =

perl_startup = do '/etc/exim.pl'
#!!# message_filter renamed system_filter
system_filter = /etc/system_filter.exim
system_filter_user = mail
system_filter_file_transport = address_file



# Specify your local domains as a colon-separated list here. If this option
# is not set (i.e. not mentioned in the configuration file), the
# qualify_recipient value is used as the only local domain. If you do not want
# to do any local deliveries, uncomment the following line, but do not supply
# any data for it. This sets local_domains to an empty string, which is not
# the same as not mentioning it at all. An empty string specifies that there
# are no local domains; not setting it at all causes the default value (the
# setting of qualify_recipient) to be used.


# If you want to accept mail addressed to your host's literal IP address, for
# example, mail addressed to "user@[111.111.111.111]", then uncomment the
# following line, or supply the literal domain(s) as part of "local_domains"
# above. You also need to comment "forbid_domain_literals" below. This is not
# recommended for today's Internet.

# local_domains_include_host_literals


# The following line prevents Exim from recognizing addresses of the form
# "user@[111.111.111.111]" that is, with a "domain literal" (an IP address)
# instead of a named domain. The RFCs still require this form, but it makes
# little sense to permit mail to be sent to specific hosts by their IP address
# in the modern Internet, and this ancient format has been used by those
# seeking to abuse hosts by using them for unwanted relaying. If you really
# do want to support domain literals, remove the following line, and see
# also the "domain_literal" router below.

#!!# forbid_domain_literals replaced by allow_domain_literals
allow_domain_literals = false


# No local deliveries will ever be run under the uids of these users (a colon-
# separated list). An attempt to do so gets changed so that it runs under the
# uid of "nobody" instead. This is a paranoic safety catch. Note the default
# setting means you cannot deliver mail addressed to root as if it were a
# normal user. This isn't usually a problem, as most sites have an alias for
# root that redirects such mail to a human administrator.

never_users = root


# The use of your host as a mail relay by any host, including the local host
# calling its own SMTP port, is locked out by default. If you want to permit
# relaying from the local host, you should set
#
# auth_hosts = *
#
# If you want to permit relaying through your host from certain hosts or IP
# networks, you need to set the option appropriately, for example
#
#
# If you are an MX backup or gateway of some kind for some domains, you must
# set relay_domains to match those domains. This will allow any host to
# relay through your host to those domains.
#


#
# See the section of the manual entitled "Control of relaying" for more
# information.


# The setting below causes Exim to do a reverse DNS lookup on all incoming
# IP calls, in order to get the true host name. If you feel this is too
# expensive, you can specify the networks for which a lookup is done, or
# remove the setting entirely.

# host_lookup = *


# By default, Exim expects all envelope addresses to be fully qualified, that
# is, they must contain both a local part and a domain. If you want to accept
# unqualified addresses (just a local part) from certain hosts, you can specify
# these hosts by setting one or both of
#
# receiver_unqualified_hosts =
# sender_unqualified_hosts =
#
# to control sender and receiver addresses, respectively. When this is done,
# unqualified addresses are qualified using the settings of qualify_domain
# and/or qualify_recipient (see above).


# By default, Exim does not make any checks, other than syntactic ones, on
# incoming addresses during the SMTP dialogue. This reduces delays in SMTP
# transactions, but it does mean that you might accept messages with unknown
# recipients, and/or bad senders.

# Uncomment this line if you want incoming recipient addresses to be verified
# during the SMTP dialogue. Unknown recipients are then rejected at this stage,
# and the generation of a failure message is the job of the sending host.

# receiver_verify

# Uncomment this line if you want incoming sender addresses (return-paths) to
# be verified during the SMTP dialogue. Verification can normally only check
# that the domain exists.

# sender_verify

# Exim contains support for the Realtime Blackhole List (RBL) that is being
# maintained as part of the DNS. See http://mail-abuse.org/rbl/ for background.
# Uncommenting the first line below will make Exim reject mail from any
# host whose IP address is blacklisted in the RBL at blackholes.mail-abuse.org.
# Some others have followed the RBL lead and have produced other lists: DUL is
# a list of dial-up addresses, and there are also a number of other lists
# of various kinds at orbs.org.

# rbl_domains = blackholes.mail-abuse.org
# rbl_domains = blackholes.mail-abuse.org:dialups.mail-abuse.org


# If you want Exim to support the "percent hack" for all your local domains,
# uncomment the following line. This is the feature by which mail addressed
# to x%y@z (where z is one of your local domains) is locally rerouted to
# x@y and sent on. Otherwise x%y is treated as an ordinary local part.

# percent_hack_domains = *


# When Exim can neither deliver a message nor return it to sender, it "freezes"
# the delivery error message (aka "bounce message"). There are also other
# circumstances in which messages get frozen. They will stay on the queue for
# ever unless one of the following options is set.

# This option unfreezes unfreezes bounce messages after two days, tries
# once more to deliver them, and ignores any delivery failures.

#!!# ignore_errmsg_errors_after renamed ignore_bounce_errors_after
ignore_bounce_errors_after = 2d

# This option cancels (removes) frozen messages that are older than a week.

timeout_frozen_after = 7d
rfc1413_query_timeout = 0s

trusted_users = mail:majordomo:apache:www:diradmin

# the code used to create the cert and key:
# openssl req -x509 -newkey rsa:1024 -keyout /etc/exim.key -out /etc/exim.cert -days 9999 -nodes
# SSL/TLS cert and key
tls_certificate = ${if exists{/etc/virtual/$smtp_active_hostname/mail.cert} {/etc/virtual/$smtp_active_hostname/mail.cert}{/etc/exim.cert} }
tls_privatekey = ${if exists{/etc/virtual/$smtp_active_hostname/mail.key} {/etc/virtual/$smtp_active_hostname/mail.key}{/etc/exim.key} }

openssl_options = +no_sslv2 +no_sslv3
tls_require_ciphers = ALL:!ADH:RC4+RSA:+HIGH:+MEDIUM:-LOW:-SSLv2:-EXP
tls_advertise_hosts = ${if or{ {eq{$smtp_active_hostname}{$primary_hostname}} {exists{/etc/virtual/$smtp_active_hostname/mail.cert}} } {*}}
#auth_over_tls_hosts = *


#!!#######################################################!!#
#!!# This new section of the configuration contains ACLs #!!#
#!!# (Access Control Lists) derived from the Exim 3      #!!#
#!!# policy control options.                             #!!#
#!!#######################################################!!#

#!!# These ACLs are crudely constructed from Exim 3 options.
#!!# They are almost certainly not optimal. You should study
#!!# them and rewrite as necessary.

begin acl

check_not_smtp:

    warn log_message = sender_ident:$sender_ident authenticated_id:$authenticated_id originator_uid:$originator_uid exim_uid:$exim_uid find_user:${perl{find_user}}

    deny set acl_c0 = ${if !eq{$originator_uid}{$exim_uid} {${if def:authenticated_id {$authenticated_id}{${perl{find_user}}}}} }
	 message = User $acl_c0 is not allowed to use SMTP
	 condition = ${if def:acl_c0 {${if exists{/etc/virtual/.smtp_deny/$acl_c0}}}}

    accept

#check_sender:
#!!# ACL that is used after the MAIL command

# deny	log_message = Sender domain rejected
#	message = Mail rejected
#	sender_domains = latinmail.com:seznam.cz:mynet.com
# accept

.include /etc/exim-greylist.conf.inc

acl_check_mail:

  accept  hosts = :

  # Hosts are required to say HELO (or EHLO) before sending mail.
  # So don't allow them to use the MAIL command if they haven't
  # done so.

  drop condition = ${if eq{$sender_helo_name}{}}
       message = Nice boys say HELO first
       log_reject_target = reject

  drop  condition = ${if match{$sender_helo_name}{$primary_hostname}}
	message   = "REJECTED - Bad HELO - Host impersonating [helo: $sender_helo_name] [host: $sender_host_address]"
	log_reject_target = reject

  drop  condition = ${if eq{[$interface_address]}{$sender_helo_name}}
	message   = "REJECTED - Interface: $interface_address is _my_ address"
	log_reject_target = reject

  drop  condition   = ${if match{$sender_helo_name}{\N\.$\N}}
        message     = Access denied - Invalid HELO name (See RFC2821 4.1.1.1)
        log_reject_target = reject

  drop  condition   = ${if match{$sender_helo_name}{\N\.\.\N}}
	message     = Access denied - Invalid HELO name (See RFC2821 4.1.1.1)
	log_reject_target = reject

  drop  message = Bad sender address
	senders = ^.*[!/|]
	log_reject_target = reject
	
  drop	hosts = +bad_sender_hosts
	message = Access from your IP ($sender_host_address) is forbidden

  deny  sender_domains = +blacklist_domains
	message = Access from your domain ($sender_address_domain) is forbidden

  deny  senders = +blacklist_senders
	message = Access from your email ($sender_address) is forbidden

  accept  hosts = +auth_relay_hosts
          authenticated = *
	  set acl_c0 = ${if def:acl_c0 {$acl_c0} {${perl{find_user}}} }
	  set acl_c1 = ${if !eq{$acl_c0}{$authenticated_id} {$authenticated_id} }
	  endpass
	  message = User $acl_c0 is not allowed to use SMTP
	  condition = ${if def:acl_c0{${if exists{/etc/virtual/.smtp_deny/$acl_c0} {no}{yes}}}{yes}}
	
  accept  !hosts = @[]
	  set acl_c1 = ${lookup{$sender_host_address}lsearch{/etc/virtual/pophosts_user}}
	  condition = ${if def:acl_c1}
	  set acl_c0 = ${if def:acl_c0 {$acl_c0} {${perl{find_user}{$acl_c1}} }}
	  condition = ${if def:acl_c0}
	  add_header = X-Authenticated-POP3: $acl_c1
	  endpass
	  message = User $acl_c0 is not allowed to use SMTP
	  condition = ${if exists{/etc/virtual/.smtp_deny/$acl_c0} {no}{yes}}

  drop  condition = ${if eq{$interface_port}{587}}
	message = Authentication required

  drop  condition   = ${if isip{$sender_helo_name}}
	message     = Access denied - Invalid HELO name (See RFC2821 4.1.3)
	log_reject_target = reject

  # Use the lack of reverse DNS to trigger greylisting. Some people
  # even reject for it but that would be a little excessive.

#  warn condition = ${if eq{$sender_host_name}{} {1}}
#       set acl_m_greylistreasons = Host $sender_host_address lacks reverse DNS\n$acl_m_greylistreasons

  accept

#!!# ACL that is used after the RCPT command
check_recipient:
  # Exim 3 had no checking on -bs messages, so for compatibility
  # we accept if the source is local SMTP (i.e. not over TCP/IP).
  # We do this by testing for an empty sending host field.

  accept  hosts = :

  drop  message = Too many failed recipients
	log_message = Too many failed recipients ($rcpt_fail_count)
	condition = ${if >={$rcpt_fail_count}{5}}
	
  deny  message = Bad local parts
	local_parts   = ^[.] : ^.*[@%!/|]
	log_reject_target = reject

  deny  domains = +local_domains
        condition = ${if exists{/etc/virtual/${domain}_off/}}
        message = Account disabled
        condition = ${if >{${eval:$tod_epoch-${extract{mtime}{${stat:/etc/virtual/${domain}_off}}}}}{432000}}
        log_reject_target = reject

  defer  domains = +local_domains
         message = Account suspended
         condition = ${if exists{/etc/virtual/${domain}_off/}}
	 log_reject_target = reject

  require   log_reject_target = reject
	    message = Unknown User
	    verify = recipient
	    set acl_m_addressdata = $address_data

  accept  hosts = +relay_hosts

  # Accept authenticated
  accept  condition = ${if def:acl_c0}

  deny  domains = !+local_domains
	message = authentication required

  defer	message = 452 Too many recipients
	condition = ${if !def:acl_c0}
	condition = ${if or{{>{$rcpt_count}{10}}{>={$rcpt_defer_count}{5}}}}
	log_reject_target = reject

  accept condition = ${if exists{/etc/virtual/${domain}/whitelist_senders}\
    	    {${lookup{$sender_address}lsearch*@{/etc/virtual/${domain}/whitelist_senders}{yes}}}}

  discard senders = !:
	  condition = ${if exists{/etc/virtual/${domain}/noreply_addresses}\
	              {${lookup{$local_part}lsearch{/etc/virtual/${domain}/noreply_addresses}{yes}}}}

  drop  hosts = !@[]
	condition = ${if !exists{/etc/virtual/${domain}/rbl_abuseat_off}}
        dnslists = cbl.abuseat.org
        message = Access denied - $sender_host_address \
                  listed by $dnslist_domain ($dnslist_value)\n$dnslist_text
	log_reject_target = reject

  drop  hosts = !@[]
	condition = ${if !exists{/etc/virtual/${domain}/rbl_sorbs_off}}
        dnslists = dnsbl.sorbs.net=127.0.0.2,127.0.0.3,127.0.0.5,127.0.0.10
        message = Access denied - $sender_host_address \
                  listed by $dnslist_domain ($dnslist_value)\n$dnslist_text
	log_reject_target = reject

  drop  hosts = !@[]
	condition = ${if exists{/etc/virtual/${domain}/rbl_spamcop}}
        dnslists = bl.spamcop.net
        message = Access denied - $sender_host_address \
                  listed by $dnslist_domain ($dnslist_value)\n$dnslist_text
	log_reject_target = reject

  drop  hosts = !@[]
	condition = ${if exists{/etc/virtual/${domain}/rbl_dulsorbsnet}}
        dnslists = dul.dnsbl.sorbs.net
        message = Access denied - $sender_host_address \
                  listed by $dnslist_domain ($dnslist_value)\n$dnslist_text
	log_reject_target = reject

  drop  !verify = sender/defer_ok
	log_reject_target = reject

  drop  hosts = !@[]
	condition = ${if !def:sender_host_name {$host_lookup_failed}}
	condition = ${if exists{/etc/virtual/${domain}/check_reverse}}
	message = Reverse DNS lookup failed
	log_reject_target = reject

  warn  condition = ${if !def:acl_c_suspect}
	condition = ${if or{ \
			{match{$sender_helo_name}{$sender_host_address}} \
			{or{{eq{$host_lookup_deferred}{1}}{!def:sender_host_name}}} \
			{match{$sender_host_name}{dialup|pool|peer|dhcp|dsl|broadband|ppp|dynamic|cable}} \
		    }}
	set acl_c_suspect = yes

  deny  condition = ${if and{{def:acl_c_suspect}{!exists{/etc/virtual/${domain}/ignore_suspicious}}{!eq{$acl_m_addressdata}{:blackhole:}}}}
	!verify = sender/defer_ok/callout=40s,connect=15s
	log_reject_target = reject


  warn  set acl_m_greylistreasons =

  warn  hosts = !@[]
	condition = ${if exists{/etc/virtual/${domain}/greylist}}
        set acl_m_greylistreasons = User prefer greylisting

  warn  condition = ${if !def:acl_m_greylistreasons}
	hosts = !@[]
	condition = ${if and{{def:acl_c_suspect}{!exists{/etc/virtual/${domain}/ignore_suspicious}}}}
	set acl_m_greylistreasons = Sender look suspicious

  warn  condition = ${if !def:acl_m_greylistreasons}
	hosts = !@[]
	senders = !:
	condition = ${if or{ \
			{eq{$acl_m_addressdata}{:blackhole:}} \
			{match{$acl_m_addressdata}{,:unknown:}} \
			{eq{$domain}{$primary_hostname}} \
			{forany{<, $acl_m_addressdata}{and{{!eq{${domain:$item}}{}}{!match_domain{${domain:$item}}{+local_domains}}}}} \
		    }}
        set acl_m_greylistreasons = We greylist some mail

  require acl = greylist_mail


  discard  condition = ${if eq{$acl_m_addressdata}{:blackhole:}}
	   log_reject_target = main

  discard  senders = :
	   condition = ${if exists{/etc/virtual/${domain}/discard_bounces}}
	   log_reject_target = main


  warn  set acl_m_dontcare = ${if eq{$domain}{$primary_hostname} {$local_part} {${lookup{$domain}lsearch{/etc/virtual/domainowners}}}}
	condition = ${if !eq{$acl_c_rcptuser}{$acl_m_dontcare}}
	set acl_c_rcptuser = $acl_m_dontcare
	set acl_c_quotauser = ${if and{{def:acl_c_rcptuser}{exists{/home/$acl_c_rcptuser/}}} \
				{${run{/usr/bin/quota "-f/home/$acl_c_rcptuser/" -q "$acl_c_rcptuser"}{}{1}}}{0}}

  defer condition = $acl_c_quotauser
	message = User quota exceeded
	log_reject_target = reject

  warn	condition = ${if !eq{$acl_c_rcpt}{$local_part@$domain}}
	set acl_m_dontcare = /home/$acl_c_rcptuser/imap/$domain/$local_part/Maildir/maildirsize
	set acl_c_rcpt = $local_part@$domain
	set acl_c_quotavirtual = ${if eq{$domain}{$primary_hostname} {no} \
				{${if exists{$acl_m_dontcare} {${perl{check_maildirsize}{$acl_m_dontcare}}}{false}}}}

  deny  condition = $acl_c_quotavirtual
	condition = ${if >{${eval:$tod_epoch-$acl_c_quotavirtual}}{432000}}
	message =  Mailbox quota exceeded for a long time
	log_reject_target = reject

  defer condition = $acl_c_quotavirtual
	message = Mailbox quota exceeded
	log_reject_target = reject


  accept  domains = +local_domains
          endpass
          verify = ${if !exists{/etc/virtual/${domain}/callout} \
	            {sender}{sender/callout=60s,connect=15s}}

  deny  message = authentication required

#!!# ACL that is used after the DATA command
#check_message:

#  accept


######################################################################
#                   AUTHENTICATION CONFIGURATION                     #
######################################################################

# There are no authenticator specifications in this default configuration file.


begin authenticators

plain:
    driver = plaintext
    public_name = PLAIN
    server_prompts = :
    server_condition = "${perl{smtpauth}}"
    server_set_id = $2

login:
    driver = plaintext
    public_name = LOGIN
    server_prompts = "Username:: : Password::"
    server_condition = "${perl{smtpauth}}"
    server_set_id = $1




######################################################################
#                      REWRITE CONFIGURATION                         #
######################################################################

# There are no rewriting specifications in this default configuration file.


#!!#######################################################!!#
#!!# Here follow routers created from the old routers,   #!!#
#!!# for handling non-local domains.                     #!!#
#!!#######################################################!!#

begin routers

check_limits:
  driver = redirect
#  domains = ! +local_domains
  domains = ! $primary_hostname
  condition = ${if def:acl_c0{${if first_delivery{${perl{check_limits}{$acl_c0}{$acl_c1}}}}}}
  no_verify
  allow_fail
  data = ":fail: You ($acl_c0) have reached your message limit"


######################################################################
#                      ROUTERS CONFIGURATION                         #
#            Specifies how remote addresses are handled              #
######################################################################
#                          ORDER DOES MATTER                         #
#  A remote address is passed to each in turn until it is accepted.  #
######################################################################

# Remote addresses are those with a domain that does not match any item
# in the "local_domains" setting above.


# This router routes to remote hosts over SMTP using a DNS lookup. Any domain
# that resolves to an IP address on the loopback interface (127.0.0.0/8) is
# treated as if it had no DNS entry.

lookuphost:
  no_verify
  driver = dnslookup
#  domains = ! +local_domains
  domains = ! $primary_hostname
  ignore_target_hosts = 127.0.0.0/8
  self = pass
  same_domain_copy_routing = true
  errors_to = ${if eq{$original_domain}{$domain} {fail}{}}
  transport = remote_smtp
  no_more

lookuphost_callout:
  verify_only
  driver = dnslookup
  domains = ! $primary_hostname
  ignore_target_hosts = 127.0.0.0/8
  self = pass
  same_domain_copy_routing = true
  transport = remote_smtp_callout
  no_more


# This router routes to remote hosts over SMTP by explicit IP address,
# when an email address is given in "domain literal" form, for example,
# <user@[192.168.35.64]>. The RFCs require this facility. However, it is
# little-known these days, and has been exploited by evil people seeking
# to abuse SMTP relays. Consequently it is commented out in the default
# configuration. If you uncomment this router, you also need to comment out
# "forbid_domain_literals" above, so that Exim can recognize the syntax of
# domain literal addresses.

# domain_literal:
#   driver = ipliteral
#   transport = remote_smtp



#!!#######################################################!!#
#!!# Here follow routers created from the old directors, #!!#
#!!# for handling local domains.                         #!!#
#!!#######################################################!!#



######################################################################
#                      DIRECTORS CONFIGURATION                       #
#             Specifies how local addresses are handled              #
######################################################################
#                          ORDER DOES MATTER                         #
#   A local address is passed to each in turn until it is accepted.  #
######################################################################

# Local addresses are those with a domain that matches some item in the
# "local_domains" setting above, or those which are passed back from the
# routers because of a "self=local" setting (not used in this configuration).

# Spam Assassin
spamcheck_director:
  driver = accept
  condition = ${if and {\
			{!def:h_X-Spam-Flag:} \
			{!eq {$received_protocol}{spam-scanned}} \
			{!eq {$received_protocol}{local}} \
			{exists {/home/${if eq {$domain} {$primary_hostname} {$local_part} \
    				{${lookup{$domain}lsearch{/etc/virtual/domainowners}}} \
			    }/.spamassassin/user_prefs}} \
		}}
  retry_use_local_part
  transport = spamcheck
  no_verify
  no_address_test

majordomo_aliases:
  driver = redirect
  allow_defer
  allow_fail
  data = ${if exists{/etc/virtual/${domain}/majordomo/list.aliases}{${lookup{$local_part}lsearch{/etc/virtual/${domain}/majordomo/list.aliases}}}}
  domains = lsearch;/etc/virtual/domainowners
  file_transport = address_file
  group = daemon
  pipe_transport = majordomo_pipe
  retry_use_local_part
  no_rewrite
  user = majordomo

majordomo_private:
  driver = redirect
  allow_defer
  allow_fail
  condition = ${if eq {$received_protocol}{local}}
  data = ${if exists{/etc/virtual/${domain}/majordomo/private.aliases}{${lookup{$local_part}lsearch{/etc/virtual/${domain}/majordomo/private.aliases}}}}
  domains = lsearch;/etc/virtual/domainowners
  file_transport = address_file
  group = daemon
  pipe_transport = majordomo_pipe
  retry_use_local_part
  user = majordomo

domain_filter:
  driver = redirect
  allow_filter
  no_check_local_user
  condition = ${if exists{/etc/virtual/${domain}/filter}}
  user = "mail"
  file = /etc/virtual/${domain}/filter
  router_home_directory = ${if match {${if exists{/etc/virtual/$domain/passwd}{${lookup{$local_part}lsearch{/etc/virtual/$domain/passwd}}}}}{.+} \
				{${extract{5}{:}{$0}}} \
				{/home/${if eq {$domain}{$primary_hostname} {$local_part}{${lookup{$domain}lsearch{/etc/virtual/domainowners}}} }} \
			}/Maildir
  directory_transport = address_file
  pipe_transport = virtual_address_pipe
  reply_transport = address_reply
  retry_use_local_part
  no_verify

#uservacation:
#   driver = accept
#   condition = ${lookup{$local_part} lsearch {/etc/virtual/${domain}/vacation.conf}{yes}{no}}
#   require_files = /etc/virtual/${domain}/reply/${local_part}.msg
#   transport = uservacation
#   unseen
#
#userautoreply:
#   driver = accept
#   condition = ${lookup{$local_part} lsearch {/etc/virtual/${domain}/autoresponder.conf}{yes}{no}}
#   require_files = /etc/virtual/${domain}/reply/${local_part}.msg
#   transport = userautoreply

virtual_aliases_nostar:
  driver = redirect
  allow_defer
  allow_fail
  condition = ${if exists{/etc/virtual/${domain}/aliases}}
  data = ${lookup{$local_part}lsearch{/etc/virtual/${domain}/aliases}}
  file_transport = address_file
  group = mail
  pipe_transport = virtual_address_pipe
  retry_use_local_part
  unseen
  #include_domain = true
  no_verify

virtual_user:
  driver = accept
  condition = ${if !eq {}{${if exists{/etc/virtual/${domain}/passwd}{${lookup{$local_part}lsearch{/etc/virtual/${domain}/passwd}}}}}}
#  condition = ${perl{save_virtual_user}} #dumb way to count future outgoing POP3 bandwidth
  domains = lsearch;/etc/virtual/domainowners
  group = mail
  retry_use_local_part
  transport = virtual_localdelivery
  no_verify

virtual_user_verify:
  driver = accept
  condition = ${if !eq {}{${if exists{/etc/virtual/${domain}/passwd}{${lookup{$local_part}lsearch{/etc/virtual/${domain}/passwd}}}}}}
  domains = lsearch;/etc/virtual/domainowners
  address_data = ${if !def:address_data {${lookup{$local_part}lsearch{/etc/virtual/${domain}/aliases}{$value,:blackhole:}}}{$address_data}}
  verify_only

virtual_aliases_verify:
  driver = redirect
  allow_defer
  allow_fail
  condition = ${if exists{/etc/virtual/$domain/aliases}}
  address_data = ${lookup{$local_part}lsearch*{/etc/virtual/$domain/aliases} \
		    # If it's not catch-all then just accept value
		    {${if eq{$1}{} {$value} \
			{${if eq{$value}{:blackhole:} {$value} \
			    # Do not accept bounces on catch-all aliases
			    {${if !def:sender_address {:fail: Your bounce seems misdirected} \
				# If routed to catch-all then add mark for greylisting
				{${if eq{${substr_0_1:$value}}{:} {$value}{$value,:unknown:} }} }} \
			    }} \
			}} }
  data = ${if eq{$address_data}{:fail:} {:fail: No such address here}{$address_data}}
  retry_use_local_part
  verify_only

virtual_aliases:
  driver = redirect
  allow_defer
  allow_fail
  condition = ${if exists{/etc/virtual/$domain/aliases}}
  data = ${lookup{$local_part}lsearch*{/etc/virtual/$domain/aliases} \
		    {${if and{{eq{$1}{}}{eq{${substr_0_2:$value}}{"|}}} \
			{:blackhole: already piped}{$value} }} }
  file_transport = address_file
  group = mail
  pipe_transport = virtual_address_pipe
  retry_use_local_part
  #include_domain = true
  no_verify

# This director handles forwarding using traditional .forward files.
# If you want it also to allow mail filtering when a forward file
# starts with the string "# Exim filter", uncomment the "filter" option.
# The check_ancestor option means that if the forward file generates an
# address that is an ancestor of the current one, the current one gets
# passed on instead. This covers the case where A is aliased to B and B
# has a .forward file pointing to A. The three transports specified at the
# end are those that are used when forwarding generates a direct delivery
# to a file, or to a pipe, or sets up an auto-reply, respectively.

system_aliases:
  driver = redirect
  domains = $primary_hostname
  allow_defer
  allow_fail
  condition = ${lookup{$local_part}lsearch{/etc/aliases}{yes}}
  address_data = ${lookup{$local_part}lsearch{/etc/aliases}}
  data = $address_data
  file_transport = address_file
  pipe_transport = address_pipe
  retry_use_local_part
  # user = exim

userforward:
  driver = redirect
  domains = $primary_hostname
  allow_filter
  check_ancestor
  check_local_user
  no_expn
  condition = ${if exists{$home/.forward}}
  file = $home/.forward
  file_transport = address_file
  pipe_transport = address_pipe
  reply_transport = address_reply
  no_verify

localuser:
  driver = accept
  domains = $primary_hostname
  check_local_user
  transport = local_delivery
  cannot_route_message = Unknown local user

# This director matches local user mailboxes.



######################################################################
#                      TRANSPORTS CONFIGURATION                      #
######################################################################
#                       ORDER DOES NOT MATTER                        #
#     Only one appropriate transport is called for each delivery.    #
######################################################################

# A transport is used only when referenced from a director or a router that
# successfully handles an address.


# Spam Assassin
begin transports

spamcheck:
  driver = pipe
  batch_max = 100
  command = /usr/local/sbin/exim -oMr spam-scanned -bS
  current_directory = "/tmp"
  group = mail
  home_directory = "/tmp"
  log_output
  message_prefix = 
  message_suffix = 
  return_fail_output
  no_return_path_add
  transport_filter = /usr/local/bin/spamc -U /var/run/spamd.sock -u "${if eq {$domain} {$primary_hostname} {$local_part} \
				{${lookup{$domain}lsearch*{/etc/virtual/domainowners}}} \
			    }"
  use_bsmtp
  user = mail
  # must use a privileged user to set $received_protocol on the way back in!


#majordomo
majordomo_pipe:
  driver = pipe
  group = daemon
  return_fail_output
  user = majordomo

# This transport is used for local delivery to user mailboxes in traditional
# BSD mailbox format. By default it will be run under the uid and gid of the
# local user, and requires the sticky bit to be set on the /var/mail directory.
# Some systems use the alternative approach of running mail deliveries under a
# particular group instead of using the sticky bit. The commented options below
# show how this can be done.

local_delivery:
  driver = appendfile
  delivery_date_add
  envelope_to_add
  directory = /home/$local_part/Maildir/
  directory_mode = 770
  create_directory = true
  maildir_format
  group = mail
  mode = 0660
  return_path_add
  user = ${local_part}
  headers_add = ${if !eq {$acl_m_greytime}{} {X-Greylist: $acl_m_greytime seconds}}

## for delivering virtual domains to their own mail spool

virtual_localdelivery:
  driver = appendfile
  create_directory
  delivery_date_add
  directory_mode = 770
  envelope_to_add
  directory = /home/${lookup{$domain}lsearch*{/etc/virtual/domainowners}}/imap/${domain}/${local_part}/Maildir
  maildir_format
  group = mail
  mode = 660
  return_path_add
  user = "${lookup{$domain}lsearch*{/etc/virtual/domainowners}}"
  quota = ${if exists{/etc/virtual/${domain}/quota}{${lookup{$local_part}lsearch*{/etc/virtual/${domain}/quota}{$value}{0}}}{0}}
  quota_is_inclusive = false
  maildir_use_size_file
  headers_add = ${if !eq {$acl_m_greytime}{} {X-Greylist: $acl_m_greytime seconds}}
  
## vacation transport
#uservacation:
#  driver = autoreply
#  file = /etc/virtual/${domain}/reply/${local_part}.msg
#  from = "${local_part}@${domain}"
#  log = /etc/virtual/${domain}/reply/${local_part}.log
#  no_return_message
#  subject = "${if def:h_Subject: {Autoreply: $h_Subject:} {I am on vacation}}"
#  text = "\
#	------                                                           ------\n\n\
#	This message was automatically generated by email software\n\
#	The delivery of your message has not been affected.\n\n\
#	------                                                           ------\n\n"
#  to = "${sender_address}"
#  user = mail
#	#once = /etc/virtual/${domain}/reply/${local_part}.once
#
#userautoreply:
#  driver = autoreply
#  bcc = ${lookup{${local_part}} lsearch {/etc/virtual/${domain}/autoresponder.conf}{$value}}
#  file = /etc/virtual/${domain}/reply/${local_part}.msg
#  from = "${local_part}@${domain}"
#  log = /etc/virtual/${domain}/reply/${local_part}.log
#  no_return_message
#  subject = "${if def:h_Subject: {Autoreply: $h_Subject:} {Autoreply Message}}"
#  to = "${sender_address}"
#  user = mail
#  #once = /etc/virtual/${domain}/reply/${local_part}.once

# This transport is used for delivering messages over SMTP connections.

remote_smtp:
  driver = smtp
  dkim_domain = $sender_address_domain
  dkim_selector = x
  dkim_private_key = ${if exists{/etc/virtual/$sender_address_domain/dkim.private.key}{/etc/virtual/$sender_address_domain/dkim.private.key}{0}}
  dkim_canon = relaxed
  dkim_strict = 0
  interface = <; ${if exists{/etc/virtual/domainips}{${lookup{$sender_address_domain}lsearch{/etc/virtual/domainips}}} {} }
  helo_data = ${if exists{/etc/virtual/helo_data}{${lookup{$sending_ip_address}iplsearch{/etc/virtual/helo_data}{$value}{$primary_hostname}}}{$primary_hostname}}
  headers_add = ${if def:authenticated_id {X-Authenticated-Id: $authenticated_id\n}}\
		${if def:authenticated_sender {X-Authenticated-Sender: $authenticated_sender\n}}\
		${if def:acl_c0 {X-Sender-Account: $acl_c0\n}}
  headers_remove = ${if and{{def::sender_address}{!def::acl_c0}{eq{${perl{log_email}{$original_local_part}{$original_domain}}}{}}} {}{}}
  return_path = ${sender_address}

remote_smtp_callout:
  driver = smtp
#  interface =

# This transport is used for handling pipe deliveries generated by alias
# or .forward files. If the pipe generates any standard output, it is returned
# to the sender of the message as a delivery error. Set return_fail_output
# instead of return_output if you want this to happen only when the pipe fails
# to complete normally. You can set different transports for aliases and
# forwards if you want to - see the references to address_pipe in the directors
# section below.

address_pipe:
  driver = pipe
  return_output

virtual_address_pipe:
  driver = pipe
  group = nobody
  return_output
  user = "${lookup{$domain}lsearch* {/etc/virtual/domainowners}}"

# This transport is used for handling deliveries directly to files that are
# generated by aliasing or forwarding.

address_file:
  driver = appendfile
  delivery_date_add
  envelope_to_add
  return_path_add

# This transport is used for handling autoreplies generated by the filtering
# option of the forwardfile director.

address_reply:
  driver = autoreply

######################################################################
#                      RETRY CONFIGURATION                           #
######################################################################

# This single retry rule applies to all domains and all errors. It specifies
# retries every 15 minutes for 2 hours, then increasing retry intervals,
# starting at 1 hour and increasing each time by a factor of 1.5, up to 16
# hours, then retries every 8 hours until 4 days have passed since the first
# failed delivery.

# Domain               Error       Retries
# ------               -----       -------

begin retry

*			*		F,2h,15m; G,16h,1h,2; F,4d,8h
*			quota		G,16h,1h,2; F,4d,8h

# End of Exim 4 configuration
