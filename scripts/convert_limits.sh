#!/bin/sh

if [ -d /etc/virtual/.smtp-limiter-old ]; then
    echo "/etc/virtual/.smtp-limiter-old directory found. conversion already done?"
    exit
fi

echo -n "global user limit: "
let GUSERLIMIT=`head -n 1 /etc/virtual/limit 2>/dev/null` 2>/dev/null
if [ 0 -ge 0$GUSERLIMIT ]; then
    echo -n "ERROR "
    let GUSERLIMIT=500
fi

echo -n "global account limit: "
let GACCLIMIT=`head -n 1 /etc/virtual/user_limit 2>/dev/null` 2>/dev/null
if [ 0 -ge 0$GACCLIMIT ]; then
    let GACCLIMIT=$GUSERLIMIT-100
    if [ ! -e /etc/virtual/user_limit ]; then
        echo "$GACCLIMIT > /etc/virtual/user_limit"
        echo $GACCLIMIT > /etc/virtual/user_limit
    fi
fi

rm -f /etc/virtual/limit_unknown

for user in `ls -1 /etc/virtual/.smtp_limit/ 2>/dev/null`; do
	if [ ! -e "/home/$user/" ]; then continue; fi
	echo -n "$user user limit: "
	let LUSERLIMIT=`head -n 1 "/etc/virtual/.smtp_limit/$user" 2>/dev/null` 2>/dev/null
	if [ 0$LUSERLIMIT -eq 1 ]; then
	    echo "$user >> /etc/virtual/blacklist_usernames"
	    echo $user >> /etc/virtual/blacklist_usernames
	    continue; 
	fi;
	cp -vp "/etc/virtual/.smtp_limit/$user" "/etc/virtual/limit_$user"
	if [ ! -n "$LUSERLIMIT" -o $LUSERLIMIT -le $GUSERLIMIT ]; then continue; fi;
	echo -n "$user account limit: "
	if [ $LUSERLIMIT -gt 200 ]; then
	    let LACCLIMIT=$LUSERLIMIT-100
	else
	    let LACCLIMIT=1+$LUSERLIMIT/2
	fi
	for pop3 in `ls -1d /home/$user/imap/*/* 2>/dev/null`; do
	    pop3=${pop3##/home/$user/imap/}
	    domain=${pop3%%/*}
	    pop3=${pop3##*/}
	    if [ ! -e "/etc/virtual/$domain/" ]; then continue; fi
	    mkdir -p /etc/virtual/$domain/limit
	    echo "$LACCLIMIT > /etc/virtual/$domain/limit/$pop3"
	    echo $LACCLIMIT > "/etc/virtual/$domain/limit/$pop3"
	done
done

for user in `find /etc/virtual/.smtp_deny/ -type f -user admin`; do
    user=${user##/etc/virtual/.smtp_deny/}
    if [ ! -e "/home/$user/" ]; then continue; fi
    grep -H "^$user\$" /etc/virtual/blacklist_usernames && continue
    echo "$user >> /etc/virtual/blacklist_usernames"
    echo $user >> /etc/virtual/blacklist_usernames
done

if [ -L /etc/virtual/usage ]; then rm /etc/virtual/usage; fi
mkdir -p /etc/virtual/usage && cp -np /etc/virtual/.smtp_usage/* /etc/virtual/usage/
mkdir -p /etc/virtual/.smtp-limiter-old && mv -v /etc/virtual/.smtp_* /etc/virtual/.smtp-limiter-old/

chown -vR mail:mail /etc/virtual/limit /etc/virtual/user_limit /etc/virtual/blacklist_usernames \
    /etc/virtual/limit_* /etc/virtual/*/limit/
chown -v mail:mail /etc/virtual/ /etc/virtual/domainowners /etc/virtual/domains
chmod -v 640 /etc/virtual/domainowners /etc/virtual/domains

echo "=== Mail limits converted  ==="
echo