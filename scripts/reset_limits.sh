#!/bin/sh
rm -r /etc/virtual/.smtp_usage.prv
mv /etc/virtual/.smtp_usage /etc/virtual/.smtp_usage.prv
cp -p /etc/virtual/.smtp_usage.prv/*.bytes /etc/virtual/usage/ 2>/dev/null
mkdir -m775 /etc/virtual/.smtp_usage && chgrp mail /etc/virtual/.smtp_usage
find /etc/virtual/.smtp_deny/ -type f ! -user admin -exec rm {} ';'
